import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os

N = 3000
Nev = 1000
U = 0.0656
D = 0.0016
H = 0.12

def get_t_forceX_forceZ_(folderName):
    forceFile = os.path.join(folderName,"postProcessing/force.dat")
    zfile = os.path.join(folderName,"zcenter")
    xfile = os.path.join(folderName,"xcenter")
    tfile = os.path.join(folderName,"time")
    forces = np.genfromtxt(forceFile,comments="#")
    zc = np.genfromtxt(zfile,comments="#")
    xc = np.genfromtxt(xfile,comments="#")
    tc = np.genfromtxt(tfile,comments="#")
    times = forces[:,0]
    forceX = forces[:,1]
    forceY = forces[:,2]
    forceZ = forces[:,3]
    t = np.linspace(times[len(times)/2],times[-1],N)
    forceZint = np.interp(t, times, forceZ)
    forceXint = np.interp(t, times, forceX)
    zint = np.interp(t, tc, zc)
    xint = np.interp(t, tc, xc)
    
    
    return t,forceXint,forceZint,xint,zint

def get_t_flowrate(folderName):
    inletFile = os.path.join(folderName,"postProcessing/flowRatePatchinlet/0/surfaceFieldValue.dat")
    outletFile = os.path.join(folderName,"postProcessing/flowRatePatchoutlet/0/surfaceFieldValue.dat")
    inletArray = np.genfromtxt(inletFile,comments="#")
    outletArray = np.genfromtxt(outletFile,comments="#")
    
    t = outletArray[:,0]
    flowout = outletArray[:,1]
    flowin = inletArray[:,1]
    
    return t,flowin,flowout
    

def main():
    

    
    folderList = ["morphing/U*5.5Fine" , "morphing/U*5.5" , "morphing/U*5.5Half" , "oversetModified/U*5.5innerBlockMeshRefine2", "oversetModified/U*5.5innerBlockMeshRefine" , "oversetModified/U*5.5innerBlockMeshRefineHalf"]
    
    fcList = [ "morphing fine", "morphing medium","morphing coarse", "overset fine", "overset medium","overset coarse"]
    
    colorList = ["b", "g", "r", "k", "c", "m"]
    
    figCl = plt.figure()
    axCl = figCl.add_subplot(1, 1, 1)
        
    figCd = plt.figure()
    axCd = figCd.add_subplot(1, 1, 1)
        
    figFr = plt.figure()
    axFr = figFr.add_subplot(1, 1, 1)
    
    figZ = plt.figure()
    axZ = figZ.add_subplot(1, 1, 1)
    
    figX = plt.figure()
    axX = figX.add_subplot(1, 1, 1)
    
    figDiffFlow = plt.figure()
    axXDiffFlow= figDiffFlow.add_subplot(1, 1, 1)
    
    for folderName,fc,colorP in zip(folderList,fcList,colorList):
                
        print "Folder analyzed: ",folderName
        
        t,forceXint,forceZint,xint,zint = get_t_forceX_forceZ_(folderName)
    
        fftFz = fftpack.fft(forceZint[Nev-1:N-1])
        freq = fftpack.fftfreq(forceZint[Nev-1:N-1].size, d=t[1]-t[0]) 
        
        fftz = fftpack.fft(zint[Nev-1:N-1])
        freqz = fftpack.fftfreq(zint[Nev-1:N-1].size, d=t[1]-t[0]) 
                
        axCl.plot(t, forceZint/(0.5*U*U*D*H), color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axCl.legend()
        axCl.set_ylim(-1,1)
        axCl.set_xlabel("t [s]")
        axCl.set_ylabel("Cl [-]")
        
        axCd.plot(t, forceXint/(0.5*U*U*D*H), color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axCd.legend()
        axCd.set_ylim(-3.5,6.5)
        #axCd.set_xlim(180,200)
        axCd.set_xlabel("t [s]")
        axCd.set_ylabel("Cd [-]")
        
        axFr.plot(freq[0:freq.size/2], np.abs(fftFz)[0:fftFz.size/2], color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axFr.legend()
        axFr.set_xlabel("fr [1/s]")
        axFr.set_ylabel("Power")
        
        axZ.plot(t, np.divide(zint,D), color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axZ.legend()
        #axZ.set_ylim(-3.5,6.5)
        #axCd.set_xlim(180,200)
        axZ.set_xlabel("t [s]")
        axZ.set_ylabel("z/D [-]")
        
        
        
        #np.savetxt("fftLift.txt",np.transpose([freq,np.abs(fftFz)]))
        print "frequency Fz with the maximum power"
        print freq[np.argmax(np.abs(fftFz))]
        
        print "frequency z with the maximum power"
        print freq[np.argmax(np.abs(fftz))]
        
        print "St number Fz with the maximum power"
        print freq[np.argmax(np.abs(fftFz))]*D/U
        
        print "rms drag coefficient"
        Cd = forceXint[Nev:N]/(0.5*U*U*D*H)
        print np.sqrt(np.mean(np.subtract(Cd,np.mean(Cd))**2))
        
        print "rms lift coefficient"
        print np.sqrt(np.mean((forceZint[Nev:N]/(0.5*U*U*D*H))**2))
        
        print "max z displacement"
        print np.max(np.divide(zint[Nev:N],D))
        
        print "rms x displacement"
        print np.sqrt(np.mean(np.subtract(xint[Nev:N],np.mean(xint[Nev:N]))**2))/D
        
        if folderName ==  "oversetModified/U*5.5innerBlockMeshRefine2" or folderName == "oversetModified/U*5.5innerBlockMeshRefine" or folderName == "oversetModified/U*5.5innerBlockMeshRefineHalf":
            t, inlet, outlet = get_t_flowrate(folderName)
            
            axXDiffFlow.plot(t, np.add(inlet,outlet)/inlet[-1]*100, color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
            axXDiffFlow.legend()
            #axXDiffFlow.set_ylim(-1,1)
            axXDiffFlow.set_xlabel("t [s]")
            axXDiffFlow.set_xlim(6.5,7)
            axXDiffFlow.set_ylabel("Flowrate difference [%]")
            
            print "max imbalance"
            print np.max(np.add(inlet[inlet.size/3:inlet.size-1],outlet[inlet.size/3:inlet.size-1])/inlet[-1]*100)
            
            print "min imbalance"
            print np.min(np.add(inlet[inlet.size/3:inlet.size-1],outlet[inlet.size/3:inlet.size-1])/inlet[-1]*100)
  
    figCl.savefig('ClAllU*55.png')
    figCd.savefig('CdAllU*55.png')
    figFr.savefig('FrAllU*55.png')
    figZ.savefig('ZU*55.png')
    figDiffFlow.savefig('FlowrateDiffU*55.png')
    
if __name__== "__main__":
  main()
    
