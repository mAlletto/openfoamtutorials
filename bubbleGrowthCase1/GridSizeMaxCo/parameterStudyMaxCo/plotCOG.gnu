set term png
set output 'cOG.png'
set xlabel 't[s]'
set ylabel 'cOG [m]'
set key left top
set parametric
set yr [0:0.018]

plot 'maxCo0.95/centerOfGravityAir.dat' u 1:2 w l tit 'maxAlphaCo = 0.95', 'maxCo0.75/centerOfGravityAir.dat' u 1:2 w l tit 'maxAlphaCo = 0.75', 'maxCo0.5/centerOfGravityAir.dat' u 1:2 w l tit 'maxAlphaCo = 0.5',  'maxCo0.25/centerOfGravityAir.dat' u 1:2 w l tit 'maxAlphaCo = 0.25', 0.684,t tit 't detachment experiment' lt 0
