set term png
set output 'cOGvarBC.png'
set xlabel 't[s]'
set ylabel 'cOG [m]'
set key left top
set parametric
set yr [0:0.018]

plot 'nOuter2nInner2fixedValueprgh/centerOfGravityAir.dat' u 1:2 w l tit 'case A', 'nOuter2nInner2prghPressure/centerOfGravityAir.dat' u 1:2 w l tit 'case B', 'nOuter2nInner2Totalprgh/centerOfGravityAir.dat' u 1:2 w l tit 'case C' , 'nOuter2nInner2prghPressurefixedValueSide/centerOfGravityAir.dat' u 1:2 w l tit 'case D', 'nOuter2nInner2prghPressureTotalPressureSide/centerOfGravityAir.dat' u 1:2 w l tit 'case E' lc 7, 'nOuter2nInner2prghPressureInletOutletUtop/centerOfGravityAir.dat' u 1:2 w l tit 'case F' lc 8,  0.684,t tit 't detachment experiment' lt 0
