set xl 't [s]'
set yl 'alpha.water/alpha.water_0 [-]'
set term 'png'
set output 'alpha.png'
set key bottom right

plot 'sphereAndBackgroundAlphaC30/postProcessing/alphaInt/0/volFieldValue.dat' u 1:($2/2.421054201563e-03) w l tit 'C30' ,  'sphereAndBackgroundAlphaC150/postProcessing/alphaInt/0/volFieldValue.dat' u 1:($2/2.421054201563e-03) w l tit 'C150'
