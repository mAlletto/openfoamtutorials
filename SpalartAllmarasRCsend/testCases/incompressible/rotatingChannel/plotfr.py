import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os
import math

def read_U_openfoam(folderName,timeStep):
    fileToProcess = os.path.join(folderName,"postProcessing/graphs",str(timeStep),"line1_U.xy")
    array = np.genfromtxt(fileToProcess,comments="#")
    y = array[:,0]
    ux = array[:,1]
    uy = array[:,2]
    uz = array[:,3]
    
    return y,ux,uy,uz

def read_fr1_openfoam(folderName,timeStep):
    fileToProcess = os.path.join(folderName,"postProcessing/graphs",str(timeStep),"line1_fr1_nuTilda_nut_rhat_rstar.xy")
    array = np.genfromtxt(fileToProcess,comments="#")
    y = array[:,0]
    fr1 = array[:,1]
    
    return y,fr1

def S12_S21(gradU):
    
    S12 = 0.5 * gradU
    S21 = 0.5 * gradU
     
    return S12,S12

def Om12_Om21(gradU,omega3):
    
    S12,S21 = S12_S21(gradU)
    Om12 =  S12 - omega3
    Om21 = -S21 + omega3
    
    return Om12,Om21

def rstar(S12,S21,Om12,Om21):
    
    S = np.sqrt(2 * np.add( np.multiply(S12,S12), np.multiply(S21,S21)) )
    Om = np.sqrt(2 * np.add( np.multiply(Om12,Om12), np.multiply(Om21,Om21)) )
    
    return np.divide(S,Om)

def HS(S12,S21,omega3):
    
    HS12 = -S12*omega3
    HS21 =  S21*omega3
    
    return HS12,HS21
    
def WSD(S12,S21,Om12,Om21):
    
    D4 = np.square (0.5 * ( 2.0* (np.square(S12) + np.square(S21)) +  2.0* (np.square(Om12) + np.square(Om21)) ) )
    
    WSD12 = 2.0 * np.divide ( np.multiply (S12 , Om12 ) , D4) 
    WSD21 = 2.0 * np.divide ( np.multiply (S21 , Om21 ) , D4)
    
    return WSD12,WSD21
    
def rhat(WSD12,WSD21,HS12,HS21):
    
    rhat = np.multiply(WSD12,2.0*HS12) + np.multiply(WSD21,2.0*HS21)
    
    return rhat

def fr1(rhat,rstar):
    
    fr1 = (1.0 + 1.0)*np.multiply(np.divide(2*rstar ,1.0 + rstar), 1.0 - np.arctan(12*rhat) ) - 1.0
    
    return fr1

def read_U_Shur():
    fileToProcess = "UrotSpalartShure2000_2.dat"
    array = np.genfromtxt(fileToProcess,comments="#",delimiter=";")
    y = array[:,0]
    ux = array[:,1]
    
    return y,ux

def read_fr1_Shur():
    fileToProcess = "fr1SpalartShure2000.dat"
    array = np.genfromtxt(fileToProcess,comments="#",delimiter=";")
    y = array[:,0]
    fr1 = array[:,1]
    
    return y,fr1



def main():
    
    timeStep = 40000
    omega3 = 0.5
    folderName = "SpalartAllmarasRC"
    
    print(timeStep)
    print(folderName)
    
    y,ux,uy,uz = read_U_openfoam(folderName,timeStep)
    urel = np.add (ux,omega3*y)
    
    yS,uxS = read_U_Shur()
    yfr1Sr,fr1Sr = read_fr1_Shur()
        
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(y, urel,color='r',label='uxrel OF',linewidth=1)
    ax.plot(yS, uxS,color='b',label='uxrel Shur',linewidth=1)
    ax.legend()
    fig.savefig('Urel.png', bbox_inches="tight")
    plt.close()
    
    gradOF = np.gradient(urel, y)
    gradShur = np.gradient(uxS, yS)
    
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(y, gradOF,color='r',label='grad OF',linewidth=1)
    ax.plot(yS, gradShur,color='b',label='grad Shur',linewidth=1)
    ax.legend()
    fig.savefig('gradUrel.png', bbox_inches="tight")
    plt.close()
    
    S12o, S21o = S12_S21(gradOF)
    Om12o, Om21o = Om12_Om21(gradOF,omega3)
    
    S12s, S21s = S12_S21(gradShur)
    Om12s, Om21s = Om12_Om21(gradShur,omega3)
    
    rstarOF = rstar(S12o,S21o,Om12o, Om21o)
    rstarShur = rstar(S12s,S21s,Om12s, Om21s)
    
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(y, rstarOF,color='r',label='rstar OF',linewidth=1)
    ax.plot(yS, rstarShur,color='b',label='rstar Shur',linewidth=1)
    ax.legend()
    fig.savefig('rstar.png', bbox_inches="tight")
    plt.close()
    
    HS12o,HS21o = HS(S12o,S21o,omega3)
    WSD12o,WSD21o = WSD(S12o,S21o,Om12o,Om21o)
    rhatOF = rhat(WSD12o,WSD21o,HS12o,HS21o)
    fr1OF = fr1(rhatOF,rstarOF)
    
    HS12s,HS21s = HS(S12s,S21s,omega3)
    WSD12s,WSD21s = WSD(S12s,S21s,Om12s,Om21s)
    rhatShur = rhat(WSD12s,WSD21s,HS12s,HS21s)
    fr1Shur = fr1(rhatShur,rstarShur)
    
    y2,fr1Solver = read_fr1_openfoam(folderName,timeStep)
    
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel(r'$y/H$')
    ax.set_ylabel(r'$f_{r1}$')
    ax.plot(y, fr1OF,color='r',label='fr1(U) OF python',linewidth=1)
    ax.plot(y2, fr1Solver,color='g',label='fr1 OF',linewidth=1)
    ax.plot(yS, fr1Shur,color='b',label='fr1(U) Shur python',linewidth=1)
    ax.plot(yfr1Sr, fr1Sr,color='k',label='fr1 Shur',linewidth=1)
    ax.legend()
    fig.savefig('fr1functionofU.png', bbox_inches="tight")
    plt.close()
    
    
    
if __name__== "__main__":
  main()
    
