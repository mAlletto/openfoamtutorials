set term 'png'
set output 'UrotChannel.png'
set xl 'y/H'  font "Helvetica,20"
set yl 'U/U_m'  font "Helvetica,20"
set tics font "Helvetica,18"
set key font "Helvetica,18"

set key bottom center

plot 'SpalartAllmaras/postProcessing/graphs/40000/line1_U.xy' u 1:(1.*($2+$1*0.5)) w l tit 'SA' lw 3 lc 1,   'SpalartAllmarasRC/postProcessing/graphs/40000/line1_U.xy' u 1:(1.*($2+$1*0.5)) w l tit 'SARC' lw 3 lc 2, 'UrotSpalartShure2000.dat' u 1:2  w l lt 1 lw 2 lc 8 tit 'Shur 2000 SARC', x+0.725 lt 0 lw 3 tit "2{/Symbol W}"

set key top right


set output 'fr1RotatingChannel.png'
set yl 'fr1'

plot 'SpalartAllmarasRC/postProcessing/graphs/40000/line1_fr1_nuTilda_nut_rhat_rstar.xy' u 1:2 w l tit 'SARC' lw 3 lc 2,  'SpalartAllmarasRCOmega0/postProcessing/graphs/40000/line1_fr1_nuTilda_nut.xy' u 1:2 w l tit 'SARC omega = 0' lw 3 lc 7 , 'fr1SpalartShure2000.dat' u 1:2  w l lt 1 lw 2 lc 8 tit 'Shur 2000 SARC' 

set output 'nutRotchannel.png
set yl '{/Symbol n}_t/{/Symbol n}'
set yr [0:30]

plot 'SpalartAllmaras/postProcessing/graphs/40000/line1_nuTilda_nut.xy' u 1:($3/1.724e-04) w l tit 'SA' lw 3,  'SpalartAllmarasRC/postProcessing/graphs/40000/line1_nuTilda_nut.xy' u 1:($3/1.724e-04) w l tit 'SARC' lw 3, 'nutSpalartShure2000.dat' u 1:2   w l lt 1 lw 2 lc 8 tit 'Shur 2000 SARC'
