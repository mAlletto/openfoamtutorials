import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os
import math

timeStepList = [4000, 5000, 6000]
colorList = ['r', 'g', 'm']

pInfty = 100000
rhoInfty = 1.17
Uinfty = math.sqrt(290**2  + 13.73**2)
b = 0.4905
c = b*0.4662
xList = [0.2, 0.4, 0.6, 0.8, 0.95]
yList = [0.0, 0.25, 0.5, 0.75, 0.95]

binwdith = 0.0025*b

def get_variable(timeStep,fileName,xPos):
    fileToProcess = os.path.join("postProcessing/samplePwall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    y = array[:,1]
    z = array[:,2]
    
    if fileName == "wallShearStress_patch_wing.raw":
        var = array[:,4]
    else:
        var = array[:,3]
    
    y = y[np.abs(x-xPos)<binwdith]
    z = z[np.abs(x-xPos)<binwdith]
    var = var[np.abs(x-xPos)<binwdith]
    
    return y,z,var

def get_variable_yPos(timeStep,fileName,yPos):
    fileToProcess = os.path.join("postProcessing/samplePwall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    y = array[:,1]
    z = array[:,2]
    
    if fileName == "wallShearStress_patch_wing.raw":
        var = array[:,4]
    else:
        var = array[:,3]
    
    x = x[np.abs(y-yPos)<binwdith]
    z = z[np.abs(y-yPos)<binwdith]
    var = var[np.abs(y-yPos)<binwdith]
    
    return x,z,var
    
    

def main():
    
    #exp = np.genfromtxt("cpset6.csv",comments="#",delimiter=',')
    #simCp = np.genfromtxt("../2012IovnovichCpMa0.725alpha4deg.csv",comments="#",delimiter=',')
    #simCf = np.genfromtxt("../2012IovnovichCfMa0.725alpha4deg.csv",comments="#",delimiter=',')
 
    
    folderName = ["./"]
     
     
    fileNameList = ["p_patch_wing.raw" , "wallShearStress_patch_wing.raw", "yPlus_patch_wing.raw"]
    
    varNameList = ["Cp","Cf","yPlus"]
    for varName,fileName in zip(varNameList,fileNameList):
        for x in xList:
            fig = plt.figure()
            for timeStep,color in zip(timeStepList,colorList):
                
                xPos = x*b
                
                ax = fig.add_subplot(1, 1, 1)
                ax.set_xlabel("x/c [-]")
                ax.set_ylabel(varName+" [-]")
                
                y,z,var = get_variable(timeStep,fileName,xPos)
                y=np.subtract(y,np.min(y))
                y=np.divide(y,np.max(y))
                
                if varName == "Cp": var = -np.divide(var-pInfty,0.5*rhoInfty*Uinfty**2)
                if varName == "Cf": var = np.divide(-var,0.5*rhoInfty*Uinfty**2)
                
                varUp = var[z>0]
                varLo = var[z<0]
                yUp = y[z>0]
                yLo = y[z<0]
                
                yUpis = yUp.argsort()
                sorted_yu = yUp[yUpis]
                sorted_vu = varUp[yUpis]
                yLois = yLo.argsort()
                sorted_yl = yLo[yLois]
                sorted_vl = varLo[yLois]

                
                ax.plot(sorted_yu, sorted_vu,color=color,label='timestep = '+str(timeStep)+', y/b = '+str(x),linewidth=1)
                #ax.plot(sorted_yl, sorted_vl,color='r',linewidth=1)
                
                if varName == "Cp":
                    exp = np.genfromtxt("Ma=0.87_alpa=26.7_x="+str(x)+".dat",comments="#")
                    ax.plot(exp[:,0],-exp[:,1],'bo',label='exp')

                
                ax.legend()
                
                
            fig.savefig(varName+'y'+str(x).replace(".","_")+'.png')
            plt.close()
                
        for y in yList:
            fig = plt.figure()
            for timeStep,color in zip(timeStepList,colorList):
                
                yPos = y*c
                
                ax = fig.add_subplot(1, 1, 1)
                ax.set_xlabel("x/c [-]")
                ax.set_ylabel(varName+" [-]")
                
                print(y)
                print(yPos)
                
                x,z,var = get_variable_yPos(timeStep,fileName,yPos)
                

                x=np.subtract(x,np.min(x))
                x=np.divide(x,np.max(x))
                
                if varName == "Cp": var = -np.divide(var-pInfty,0.5*rhoInfty*Uinfty**2)
                if varName == "Cf": var = np.divide(-var,0.5*rhoInfty*Uinfty**2)
                
                varUp = var[z>0]
                varLo = var[z<0]
                xUp = x[z>0]
                xLo = x[z<0]
                
                xUpis = xUp.argsort()
                sorted_xu = xUp[xUpis]
                sorted_vu = varUp[xUpis]
                xLois = xLo.argsort()
                sorted_xl = xLo[xLois]
                sorted_vl = varLo[xLois]

                
                ax.plot(sorted_xu, sorted_vu,color=color,label='timestep ='+str(timeStep)+'y/b = '+str(y),linewidth=1)
                #ax.plot(sorted_yl, sorted_vl,color='r',linewidth=1)
                
                
                ax.legend()
                
                
            fig.savefig(varName+'x'+str(y).replace(".","_")+'.png')
            plt.close()
        
    
    
    
if __name__== "__main__":
  main()
    
