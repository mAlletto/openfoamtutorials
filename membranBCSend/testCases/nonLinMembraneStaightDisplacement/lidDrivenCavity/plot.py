import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import xarray as xr
import os
import math

timeStep = 500

pInfty = 100000
rhoInfty = 1.17
Uinfty = math.sqrt(290**2  + 13.73**2)
b = 1.1963
yList = [0.2, 0.44, 0.65, 0.8, 0.9, 0.96, 0.99]
binwdith = 0.0025*b

def get_variable(folderName,timeStep,fileName):
    fileToProcess = os.path.join(folderName,"postProcessing/sampleWall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    y = array[:,1]
    z = array[:,2]
    var = array[:,3]
    
    return x,y,z,var
    
    

def main():
    
    GebAndVi = np.genfromtxt("./2003GerberauAndVidrasu.dat",comments="#",delimiter=';')
    Vasquez = np.genfromtxt("./2007Vasquez.dat",comments="#",delimiter=';')
    Kass = np.genfromtxt("./2011Kassiotisetal.dat",comments="#",delimiter=';')
 
    
    folderName = "./postProcessing/sampleWall/surface"
     
    fileName  = "p_patch_membrane.raw"
    
    
    
    folders  = ["cavityN20", "cavityN40","cavityN40steady"]
    colors  = ["r", "y","c"]
    
    
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel("t [s]")
    ax.set_ylabel("y [m]")
    ax.plot(GebAndVi[:,0],GebAndVi[:,1],color='b',label='2003 Gerbeau and Vidrascu')
    ax.plot(Kass[:,0],Kass[:,1],color='g',label='2011 Kassiotis et al.')
    ax.plot(Vasquez[:,0],Vasquez[:,1],color='m',label='2007 Valdes Vazquez')
    
    for folder,color in zip(folders,colors):
        items = os.listdir(os.path.join(folder,folderName))
        xa = []
        ya = []
        za = []
        pa = []
        ta = []
        for item in items:
        # if os.path.isdir(item):
            #print(item)
            
            x,y,z,p = get_variable(folder,item,fileName)
            xa.append(x)
            ya.append(y)
            za.append(z)
            pa.append(p)
            ta.append(float(item))
            
        data = np.transpose(np.array(ya))
        coord = xa[0]
        time = ta
        
        da = xr.DataArray(data=data,dims=["x", "time"], coords=[xa[0],time])
        yc = da.interp(x=0.5, method="nearest")
        ax.plot(yc["time"],yc.data,color=color,label=folder,linewidth=1)
  
    ax.legend()
    fig.savefig('displacement_cavity.png')
    plt.close()

    
    
if __name__== "__main__":
  main()
    
