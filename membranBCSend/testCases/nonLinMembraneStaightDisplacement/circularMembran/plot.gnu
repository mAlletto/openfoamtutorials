set term png
set output 'comparisonPukovich2015.png'
set xlabel 'r [mm]'
set ylabel 'w [mm]'
set datafile separator ", | whitespace"

plot 'circularMembrane70Pa/VTK/finite-area/w.txt' u (($3-0.1)*1000):(-$2*1000) w l tit 'Sim 70 Pa', 'circularMembrane70PaFine/VTK/finite-area/w.txt' u (($3-0.1)*1000):(-$2*1000) w l tit 'Sim 70 Pa Fine', 'DisplacementMembranPukovich2015_70Pa.txt' u 1:2 w p pt 6 tit 'Exp 70 Pa' , 'circularMembrane230Pa/VTK/finite-area/w.txt' u (($3-0.1)*1000):(-$2*1000) w l tit 'Sim 230 Pa', 'circularMembrane230PaFine/VTK/finite-area/w.txt' u (($3-0.1)*1000):(-$2*1000) w l tit 'Sim 230 Pa Fine', 'DisplacementMembranPukovich2015_230Pa.txt' u 1:2 w p pt 6 tit 'Exp 230 Pa'

