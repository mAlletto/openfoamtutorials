/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2015 OpenFOAM Foundation
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::membraneDisplacementPointPatchVectorField

Description
    pointe displacement according to membrane theory
 
SourceFiles
    membraneDisplacementPointPatchVectorField.C

\*---------------------------------------------------------------------------*/

#ifndef membraneDisplacementPointPatchVectorField_H
#define membraneDisplacementPointPatchVectorField_H

#include "pointPatchFields.H"
#include "fixedValuePointPatchFields.H"
#include "searchableSurfaces.H"
#include "membraneModel.H"
#include "autoPtr.H"
#include "volFieldsFwd.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
          Class membraneDisplacementPointPatchVectorField Declaration
\*---------------------------------------------------------------------------*/

class membraneDisplacementPointPatchVectorField
:
    public fixedValuePointPatchVectorField
{
    
    // Typedefs

        //- The finite-area region model
        typedef regionModels::membraneModel baffleType;


    // Private Data

        //- The membrane model
        autoPtr<baffleType> baffle_;

        //- Dictionary
        dictionary dict_;
        
        // undeformed nomral
        const vectorField undeformedNormal_;
        
        // undeformed nomral
        vectorField oldNormal_;
        
        // use undeformed nomral
        bool useUndeformedNormal_;

private:

    // Private data


    // Private Member Functions


        //- No copy assignment
        void operator=
        (
            const membraneDisplacementPointPatchVectorField&
        ) = delete;


public:

    //- Runtime type information
    TypeName("membraneDisplacement");


    // Constructors

        //- Construct from patch and internal field
        membraneDisplacementPointPatchVectorField
        (
            const pointPatch&,
            const DimensionedField<vector, pointMesh>&
        );

        //- Construct from patch, internal field and dictionary
        membraneDisplacementPointPatchVectorField
        (
            const pointPatch&,
            const DimensionedField<vector, pointMesh>&,
            const dictionary&
        );

        //- Construct by mapping given patchField<vector> onto a new patch
        membraneDisplacementPointPatchVectorField
        (
            const membraneDisplacementPointPatchVectorField&,
            const pointPatch&,
            const DimensionedField<vector, pointMesh>&,
            const pointPatchFieldMapper&
        );

        //- Construct as copy
        membraneDisplacementPointPatchVectorField
        (
            const membraneDisplacementPointPatchVectorField&
        );

        //- Construct and return a clone
        virtual autoPtr<pointPatchVectorField> clone() const
        {
            return autoPtr<pointPatchVectorField>
            (
                new membraneDisplacementPointPatchVectorField
                (
                    *this
                )
            );
        }

        //- Construct as copy setting internal field reference
        membraneDisplacementPointPatchVectorField
        (
            const membraneDisplacementPointPatchVectorField&,
            const DimensionedField<vector, pointMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual autoPtr<pointPatchVectorField> clone
        (
            const DimensionedField<vector, pointMesh>& iF
        ) const
        {
            return autoPtr<pointPatchVectorField>
            (
                new membraneDisplacementPointPatchVectorField
                (
                    *this,
                    iF
                )
            );
        }

    // Member Functions

        //- Update the coefficients associated with the patch field
        virtual void updateCoeffs();

        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
