/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2019-2021 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "isotropicPrestressMembraneModel.H"
#include "addToRunTimeSelectionTable.H"
#include "fvPatchFields.H"
#include "zeroGradientFaPatchFields.H"
#include "subCycle.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace regionModels
{

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

defineTypeNameAndDebug(isotropicPrestressMembraneModel, 0);

addToRunTimeSelectionTable(membraneModel, isotropicPrestressMembraneModel, dictionary);

// * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

bool isotropicPrestressMembraneModel::init(const dictionary& dict)
{
    this->solution().readEntry("nNonOrthCorr", nNonOrthCorr_);
    return true;
}

// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //

void isotropicPrestressMembraneModel::solveDisplacement()
{
    if (debug)
    {
        InfoInFunction << endl;
    }


    areaScalarField solidMass(rho()*h_);
    areaScalarField solidD(D()/solidMass);

    // use coefficients of the initial (undeformed) configuration
    // it is consistent with the linear theory
    //static faScalarMatrix laplaceUndeformed(fam::laplacian(w_));
    //tmp<faScalarMatrix> tlaplaceUndeformed = laplaceUndeformed;


    faScalarMatrix wEqn
    (
        
        //fam::laplacian(w_) 
        laplaceUndeformed_
        ==
        fam::d2dt2(rho()/sigma0_, w_) - ps()/(sigma0_*h_)
        + faOptions()(rho()/sigma0_, w_, dimDensity/dimPressure*dimLength/sqr(dimTime))
    );
    

    faOptions().constrain(wEqn);

    wEqn.solve();


    Info<< w_.name() << " min/max   = " << gMinMax(w_) << endl;


    faOptions().correct(w_);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

isotropicPrestressMembraneModel::isotropicPrestressMembraneModel
(
    const word& modelType,
    const fvMesh& mesh,
    const dictionary& dict
)
:
    membraneModel(modelType, mesh, dict),
    sigma0_("sigma0", dimPressure, dict),
    nNonOrthCorr_(1),
    ps_
    (
        IOobject
        (
            "ps_" + regionName_,
            primaryMesh().time().timeName(),
            primaryMesh(),
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        regionMesh(),
        dimensionedScalar(dimPressure, Zero)
    ),
    h_
    (
        IOobject
        (
            "h_" + regionName_,
            primaryMesh().time().timeName(),
            primaryMesh(),
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        regionMesh()
    ),
    laplaceUndeformed_(fam::laplacian(w_))
{
    Info << "isotropicPrestressMembraneModel" << endl;
    init(dict);
}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void isotropicPrestressMembraneModel::preEvolveRegion()
{}


void isotropicPrestressMembraneModel::evolveRegion()
{
    nNonOrthCorr_ = solution().get<label>("nNonOrthCorr");
    
    if (primaryMesh().moving())
    {
        regionMesh().movePoints();
    }

    for (int nonOrth=0; nonOrth<=nNonOrthCorr_; nonOrth++)
    {
        solveDisplacement();
    }
    

}


const tmp<areaScalarField> isotropicPrestressMembraneModel::D() const
{
    const dimensionedScalar E("E", dimForce/dimArea , solid().E());
    const dimensionedScalar nu("nu", dimless, solid().nu());

    return tmp<areaScalarField>(E*pow3(h_)/(12*(1 - sqr(nu))));
}


const tmp<areaScalarField> isotropicPrestressMembraneModel::rho() const
{
    return tmp<areaScalarField>
    (
        new areaScalarField
        (
            IOobject
            (
                "rhos",
                primaryMesh().time().timeName(),
                primaryMesh(),
                IOobject::NO_READ,
                IOobject::NO_WRITE,
                false
            ),
            regionMesh(),
            dimensionedScalar("rho", dimDensity, solid().rho()),
            zeroGradientFaPatchScalarField::typeName
        )
    );
}

void isotropicPrestressMembraneModel::info()
{}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace regionModels
} // End namespace Foam

// ************************************************************************* //
