/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2006                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    "pcorr.*"
    {
        solver           GAMG;
        tolerance        0.02;
        relTol           0;
        smoother         GaussSeidel;
    }

    p
    {
        $pcorr;
        tolerance        1e-7;
        relTol           0.01;
    }

    pFinal
    {
        $p;
        tolerance        1e-7;
        relTol           0.01;
    }

    "(U|k|omega|gammaInt|ReThetat)"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-06;
        relTol          0.01;
    }

    "(U|k|omega|gammaInt|ReThetat)Final"
    {
        $U;
        tolerance       1e-06;
        relTol          0.01;
    }

    cellDisplacement
    {
        solver          GAMG;
        tolerance       1e-5;
        relTol          0;
        smoother        GaussSeidel;
    }
}

PIMPLE
{
    correctPhi          yes;
    nOuterCorrectors    2;
    nCorrectors         1;
    nNonOrthogonalCorrectors 0;
    consistent yes;
}

relaxationFactors
{
    fields
    {
        p               1.;
    }
    equations
    {
        "(U|k|omega)"   1.;
        "(U|k|omega)Final" 1.0;
    }
}

cache
{
    grad(U);
}

// ************************************************************************* //
