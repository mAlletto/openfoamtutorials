set xl 't [s]'
set yl 'Cl [-]'

magU = sqrt(250.5**2 + 17.97**2)
rho = 1.17
A = 0.1

set term png
set output 'Clgrid.png'

set key right bottom

plot "< sed s/[\\(\\)]//g grid0/postProcessing/forces1/0/force.dat" u 1:($4/(0.5*magU**2*rho*A)) w l tit 'grid0' , "< sed s/[\\(\\)]//g grid1/postProcessing/forces1/0/force.dat" u 1:($4/(0.5*magU**2*rho*A)) w l tit 'grid1' , "< sed s/[\\(\\)]//g grid2/postProcessing/forces1/0/force.dat" u 1:($4/(0.5*magU**2*rho*A)) w l tit 'grid2' , "< sed s/[\\(\\)]//g grid3/postProcessing/forces1/0/force.dat" u 1:($4/(0.5*magU**2*rho*A)) w l tit 'grid3'

