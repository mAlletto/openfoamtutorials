import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os
import math

timeStep = 0.22

pInfty = 100000
rhoInfty = 1.17
UinftyMa0725 = math.sqrt(250.5**2  + 17.97**2)
UinftyMa075 = math.sqrt(259.5**2 +   8.97**2 )
UinftyMa0775 = math.sqrt(268**2 +  8.975**2)
UinftyMa08 = math.sqrt(276.5**2  +  4.485**2)

def get_variable(folderName,timeStep,fileName):
    fileToProcess = os.path.join(folderName,"postProcessing/samplePwall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    z = array[:,2]
    var = array[:,3]
    
    return x,z,var
    
    

def main():
    
    exp1degM08 = np.genfromtxt("../Ma0.8angle1deg.csv",comments="#",delimiter=',')
    exp2degM075 = np.genfromtxt("../Ma0.75angle2deg.csv",comments="#",delimiter=',')
    exp2degM0775 = np.genfromtxt("../Ma0.775angle2deg.csv",comments="#",delimiter=',')
    exp4degM0725 = np.genfromtxt("../Ma0.725angle4deg.csv",comments="#",delimiter=',')
    
    sim1degM08Cp = np.genfromtxt("../2012IovnovichCpMa0.8alpha1deg.csv",comments="#",delimiter=',')
    sim2degM075Cp = np.genfromtxt("../2012IovnovichCpMa0.75alpha2deg.csv",comments="#",delimiter=',')
    sim2degM075Cf = np.genfromtxt("../2012IovnovichCfMa0.75alpha2deg.csv",comments="#",delimiter=',')
    sim4degM0725Cp = np.genfromtxt("../2012IovnovichCpMa0.725alpha4deg.csv",comments="#",delimiter=',')
    sim4degM0725Cf = np.genfromtxt("../2012IovnovichCfMa0.725alpha4deg.csv",comments="#",delimiter=',')
    
    cpExpList = [exp1degM08,exp2degM075,exp2degM0775,exp4degM0725]
    
    cpSimList = [sim1degM08Cp,sim2degM075Cp,"",sim4degM0725Cp]
    cfSimList = ["",sim2degM075Cf,"",sim4degM0725Cf]
    
    folderList = ["1degreeM0.8" , "2degreeM0.75" , "2degreeM0.775" , "4degreeM0.725"]
    
    UinftyList = [UinftyMa08 , UinftyMa0775 , UinftyMa075 , UinftyMa0725]
            
    fileNameList = ["p_patch_aerofoil.raw" , "wallShearStress_patch_aerofoil.raw", "yPlus_patch_aerofoil.raw"]
    
    varNameList = ["Cp","Cf","yPlus"]
    
    colorList = ["b", "g", "r", "k"]
    
    figCp = plt.figure()
    axCp = figCp.add_subplot(1, 1, 1)
    axCp.set_xlabel("x/c [-]")
    axCp.set_ylabel("Cp [-]")

    figCf = plt.figure()
    axCf = figCf.add_subplot(1, 1, 1)
    axCf.set_xlabel("x/c [-]")
    axCf.set_ylabel("Cf [-]")
        
    figYp = plt.figure()
    axYp = figYp.add_subplot(1, 1, 1)
    axYp.set_xlabel("x/c [-]")
    axYp.set_ylabel("yPlus [-]")
    
    figList = [figCp,figCf,figYp]
    axList = [axCp,axCf,axYp]

    
    for varName,fileName,fig,ax in zip(varNameList,fileNameList,figList,axList):
        for folderName,colorP,Uinfty,cpExp,cpSim,cfSim in zip(folderList,colorList,UinftyList,cpExpList,cpSimList,cfSimList):
            x,z,var = get_variable(folderName,timeStep,fileName)
            
            if varName == "Cp": var = -np.divide(var-pInfty,0.5*rhoInfty*Uinfty**2)
            if varName == "Cf": var = np.divide(-var,0.5*rhoInfty*Uinfty**2)
            
            varUp = var[z>0]
            varLo = var[z<0]
            xUp = x[z>0]
            xLo = x[z<0]
            
            xUpis = xUp.argsort()
            sorted_xu = xUp[xUpis]
            sorted_vu = varUp[xUpis]
            xLois = xLo.argsort()
            sorted_xl = xLo[xLois]
            sorted_vl = varLo[xLois]

            ax.plot(sorted_xl, sorted_vl, color=colorP,label=folderName,linewidth=1)
            ax.plot(sorted_xu, sorted_vu, color=colorP,linewidth=1)
            
            if varName == "Cp":
                figCpComp = plt.figure()
                axCpComp = figCpComp.add_subplot(1, 1, 1)
                axCpComp.set_xlabel("x/c [-]")
                axCpComp.set_ylabel("Cp [-]")
                axCpComp.plot(sorted_xl, sorted_vl, color=colorP,label=folderName,linewidth=1)
                axCpComp.plot(sorted_xu, sorted_vu, color=colorP,linewidth=1)
                axCpComp.plot(cpExp[:,0],cpExp[:,1],'bo',label="exp")
                if cpSim != "":
                    axCpComp.plot(cpSim[:,0],cpSim[:,1],'rv',label="ref sim")
                axCpComp.legend()
                figCpComp.savefig(folderName+varName+'.png')

            if varName == "Cf":
                figCfComp = plt.figure()
                axCfComp = figCfComp.add_subplot(1, 1, 1)
                axCfComp.set_xlabel("x/c [-]")
                axCfComp.set_ylabel("Cf [-]")
                axCfComp.plot(sorted_xl, sorted_vl, color=colorP,label=folderName,linewidth=1)
                axCfComp.plot(sorted_xu, sorted_vu, color=colorP,linewidth=1)
                if cfSim != "":
                    axCfComp.plot(cfSim[:,0],cfSim[:,1],'rv',label="ref sim")
                axCfComp.legend()
                figCfComp.savefig(folderName+varName+'.png')
            
            
            
        #if varName == "Cp":
        #    ax.plot(exp[:,0],exp[:,1],'bo',label="exp")
        ax.legend()
            
            
        fig.savefig(varName+'angleMaStudy'+'.png')
        
    
    
    
if __name__== "__main__":
  main()
    
