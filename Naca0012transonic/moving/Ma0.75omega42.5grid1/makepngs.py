#### import the simple module from the paraview
from paraview.simple import *
import os
import glob

def findtimeStepIndex (timeStep,directory='.'):
    listDirs = next(os.walk(directory))[1]
    timeStepList = [ a for a in listDirs if a.replace('.', '', 1).isdigit() ]
    timeStepList.sort()
    for tsDir in timeStepList:
        if tsDir == str(timeStep):
            return timeStepList.index(tsDir)



degList =      [0.52 ,  1.09 , 2.01 , 2.34 , -0.54 , -1.25 , -2.0 ,  -2.41]
timeStepList = [ 0.217, 0.158, 0.2, 0.193,  0.142,  0.234,  0.273, 0.264]


for ts,deg in zip(timeStepList,degList):

    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # create a new 'OpenFOAMReader'
    mfoam = OpenFOAMReader(FileName='m.foam')
    mfoam.MeshRegions = ['internalMesh']

    variablesList = ['U']

    mfoam.CellArrays = variablesList

    # get animation scene
    animationScene1 = GetAnimationScene()

    # get time steps
    tk = GetTimeKeeper()
    timesteps = tk.TimestepValues
    
    animationScene1.AnimationTime = timesteps[findtimeStepIndex(ts)-1]

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    # show data in view
    mfoamDisplay = Show(mfoam, renderView1)

    # show color bar/color legend
    mfoamDisplay.SetScalarBarVisibility(renderView1, True)

    # update the view to ensure updated data information
    renderView1.Update()


    # set scalar coloring
    ColorBy(mfoamDisplay, ('CELLS', 'U', 'Magnitude'))

    # Hide the scalar bar for this color map if no visible data is colored by it.
    #HideScalarBarIfNotNeeded(pLUT, renderView1)

    # rescale color and/or opacity maps used to include current data range
    mfoamDisplay.RescaleTransferFunctionToDataRange(True, False)

    # show color bar/color legend
    mfoamDisplay.SetScalarBarVisibility(renderView1, True)

    # get color transfer function/color map for 'U'
    uLUT = GetColorTransferFunction('U')
    uLUT.RGBPoints = [4.998669803574499, 0.231373, 0.298039, 0.752941, 158.36589672118356, 0.865003, 0.865003, 0.865003, 311.7331236387926, 0.705882, 0.0156863, 0.14902]
    uLUT.ScalarRangeInitialized = 1.0

    # get opacity transfer function/opacity map for 'U'
    uPWF = GetOpacityTransferFunction('U')
    uPWF.Points = [4.998669803574499, 0.0, 0.5, 0.0, 311.7331236387926, 1.0, 0.5, 0.0]
    uPWF.ScalarRangeInitialized = 1

    # Rescale transfer function
    uLUT.RescaleTransferFunction(0.0, 400.0)
    UpdateScalarBarsComponentTitle(uLUT,mfoamDisplay)

    # Rescale transfer function
    uPWF.RescaleTransferFunction(0.0, 400.0)
    UpdateScalarBarsComponentTitle(uPWF,mfoamDisplay)
    
    # get color legend/bar for uLUT in view renderView1
    uLUTColorBar = GetScalarBar(uLUT, renderView1)
    uLUTColorBar.Title = 'U'
    uLUTColorBar.ComponentTitle = 'Magnitude'
    uLUTColorBar.TitleFontFile = ''
    uLUTColorBar.LabelFontFile = ''

    # change scalar bar placement
    uLUTColorBar.WindowLocation = 'AnyLocation'
    uLUTColorBar.Position = [0.6702544031311154, 0.04148471615720528]
    # change scalar bar placement
    uLUTColorBar.Orientation = 'Horizontal'
    uLUTColorBar.Position = [0.1, 0.65]
    uLUTColorBar.ScalarBarLength = 0.5
    
    if deg != -2.0:
        uLUTColorBar.Position = [-1.0, -1.0]
    
    # rescale color and/or opacity maps used to exactly fit the current data range
    #mfoamDisplay.RescaleTransferFunctionToDataRange(False, True)

    # current camera placement for renderView1
    renderView1.CameraPosition = [0.425752478883755, -1.8385035478942764, 0.006127841341188489]
    renderView1.CameraFocalPoint = [0.425752478883755, -0.05000000074505806, 0.006127841341188489]
    renderView1.CameraViewUp = [0.0, 0.0, 1.0]
    renderView1.CameraParallelScale = 25.350049578226546

#    renderView1.Update()
    # save screenshot
    SaveScreenshot('U_deg'+str(deg).replace('.','')+'.png', renderView1, ImageResolution=[1280, 800])
