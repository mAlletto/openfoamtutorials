# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
mfoam = OpenFOAMReader(FileName='/home/michael/suse42/michael/TESTCASES/OF1912/Naca0012send/moving/Ma0.75omega42.5grid1/m.foam')
mfoam.MeshRegions = ['internalMesh']
mfoam.CellArrays = ['Ma', 'T', 'U', 'alphat', 'cellDisplacement', 'dpdt', 'nuTilda', 'nut', 'p', 'rho', 'wallShearStress', 'yPlus']
mfoam.PointArrays = ['pointDisplacement']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1022, 458]

# show data in view
mfoamDisplay = Show(mfoam, renderView1)

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')
pLUT.RGBPoints = [89054.265625, 0.231373, 0.298039, 0.752941, 123038.6171875, 0.865003, 0.865003, 0.865003, 157022.96875, 0.705882, 0.0156863, 0.14902]
pLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')
pPWF.Points = [89054.265625, 0.0, 0.5, 0.0, 157022.96875, 1.0, 0.5, 0.0]
pPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
mfoamDisplay.Representation = 'Surface'
mfoamDisplay.ColorArrayName = ['POINTS', 'p']
mfoamDisplay.LookupTable = pLUT
mfoamDisplay.OSPRayScaleArray = 'p'
mfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
mfoamDisplay.SelectOrientationVectors = 'U'
mfoamDisplay.ScaleFactor = 3.6
mfoamDisplay.SelectScaleArray = 'p'
mfoamDisplay.GlyphType = 'Arrow'
mfoamDisplay.GlyphTableIndexArray = 'p'
mfoamDisplay.GaussianRadius = 0.18
mfoamDisplay.SetScaleArray = ['POINTS', 'p']
mfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
mfoamDisplay.OpacityArray = ['POINTS', 'p']
mfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
mfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
mfoamDisplay.SelectionCellLabelFontFile = ''
mfoamDisplay.SelectionPointLabelFontFile = ''
mfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
mfoamDisplay.ScalarOpacityFunction = pPWF
mfoamDisplay.ScalarOpacityUnitDistance = 1.867807622765641

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
mfoamDisplay.DataAxesGrid.XTitleFontFile = ''
mfoamDisplay.DataAxesGrid.YTitleFontFile = ''
mfoamDisplay.DataAxesGrid.ZTitleFontFile = ''
mfoamDisplay.DataAxesGrid.XLabelFontFile = ''
mfoamDisplay.DataAxesGrid.YLabelFontFile = ''
mfoamDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
mfoamDisplay.PolarAxes.PolarAxisTitleFontFile = ''
mfoamDisplay.PolarAxes.PolarAxisLabelFontFile = ''
mfoamDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
mfoamDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# show color bar/color legend
mfoamDisplay.SetScalarBarVisibility(renderView1, True)

renderView1.ViewTime = 10000.0
renderView1.CacheKey = 10000.0
rRenderView1.UseCache = 0

# update the view to ensure updated data information
renderView1.Update()

# reset view to fit data
renderView1.ResetCamera()

# set scalar coloring
ColorBy(mfoamDisplay, ('CELLS', 'U', 'Magnitude'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(pLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
mfoamDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
mfoamDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
uLUT.RGBPoints = [12.070910361730602, 0.231373, 0.298039, 0.752941, 161.96236829924285, 0.865003, 0.865003, 0.865003, 311.85382623675514, 0.705882, 0.0156863, 0.14902]
uLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')
uPWF.Points = [12.070910361730602, 0.0, 0.5, 0.0, 311.85382623675514, 1.0, 0.5, 0.0]
uPWF.ScalarRangeInitialized = 1

# rescale color and/or opacity maps used to exactly fit the current data range
mfoamDisplay.RescaleTransferFunctionToDataRange(False, True)

# current camera placement for renderView1
renderView1.CameraPosition = [0.425752478883755, -1.8385035478942764, 0.006127841341188489]
renderView1.CameraFocalPoint = [0.425752478883755, -0.05000000074505806, 0.006127841341188489]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 25.350049578226546

# save screenshot
SaveScreenshot('/home/michael/suse42/michael/TESTCASES/OF1912/Naca0012send/moving/U_052deg.png', renderView1, ImageResolution=[1022, 458])

# current camera placement for renderView1
renderView1.CameraPosition = [0.43674227653545206, -1.8366902152413909, -0.018407990143932886]
renderView1.CameraFocalPoint = [0.42574778439165845, -0.048388763981794855, 0.006127264708197719]
renderView1.CameraViewUp = [8.433871977447905e-05, -0.013718055640721765, 0.9999058994907564]
renderView1.CameraParallelScale = 25.350049578226546

# save screenshot
SaveScreenshot('/home/michael/suse42/michael/TESTCASES/OF1912/Naca0012send/moving/U_109deg.png', renderView1, ImageResolution=[1022, 458])