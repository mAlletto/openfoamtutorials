import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os
import math


pInfty = 100000
rhoInfty = 1.17
Uinfty = math.sqrt(261.3**2  + 0.07**2)

def get_variable(folderName,timeStep,fileName):
    fileToProcess = os.path.join(folderName,"postProcessing/samplePwall/surface",str(timeStep),fileName)
    array = np.genfromtxt(fileToProcess,comments="#")
    
    x = array[:,0]
    z = array[:,2]
    var = array[:,3]
    
    return x,z,var
    
    

def main():
    

    expUpperListcsv = ["0.52degUpper.csv" , "1.09degUpper.csv" , "2.01degUpper.csv" , "2.34degUpper.csv" , "-0.54degUpper.csv" , "-1.25degUpper.csv" , "-2.0degUpper.csv" , "-2.41degUpper.csv"]
    
    expLowerListcsv = ["0.52degLower.csv" , "1.09degLower.csv" , "2.01degLower.csv" , "2.34degLower.csv" , "-0.54degLower.csv" , "-1.25degLower.csv" , "-2.0degLower.csv" , "-2.41degLower.csv"]
    
    degList =      [0.52 ,  1.09 , 2.01 , 2.34 , -0.54 , -1.25 , -2.0 ,  -2.41]
    timeStepList = [ 0.217, 0.158, 0.2, 0.193,  0.142,  0.234,  0.273, 0.264]
    
    forecCoeffSimFile = "postProcessing/forceCoeffs1/0/coefficient.dat"
    forecCoeffExpFile = "Clmoving.csv"
    momentCoeffExpFile = "Cmmoving.csv"
    
    Clexp = np.genfromtxt(forecCoeffExpFile,comments="#",delimiter=',')
    Cmexp = np.genfromtxt(momentCoeffExpFile,comments="#",delimiter=',')

    
    expUpnp = []
    expLonp = []
    
    for up,low in zip(expUpperListcsv,expLowerListcsv):
        expUpnp.append(np.genfromtxt(up,comments="#",delimiter=','))
        expLonp.append(np.genfromtxt(low,comments="#",delimiter=','))
    
    
    folderList = ["Ma0.75omega42.5grid2" ,"Ma0.75omega42.5grid1", "Ma0.75omega42.5grid1kOmegaSST","Ma0.75omega42.5grid11Orderp"]
            
    fileNameList = ["p_patch_aerofoil.raw" , "wallShearStress_patch_aerofoil.raw", "yPlus_patch_aerofoil.raw"]
    
    varNameList = ["Cp","Cf","yPlus"]
    
    colorList = ["b", "g", "r", "k"]
    
    linestyleList = ['-', '--', '-.', ':']
    


    for deg,timeStep,expUp,expLo in zip(degList,timeStepList,expUpnp,expLonp):
        figCp = plt.figure()
        axCp = figCp.add_subplot(1, 1, 1)
        axCp.set_ylim(-1.5,1.5)
            
        figCf = plt.figure()
        axCf = figCf.add_subplot(1, 1, 1)
            
        figYp = plt.figure()
        axYp = figYp.add_subplot(1, 1, 1)
        
        figList = [figCp,figCf,figYp]
        axList = [axCp,axCf,axYp]
        for varName,fileName,fig,ax in zip(varNameList,fileNameList,figList,axList):
            for folderName,linestyleL in zip(folderList,linestyleList):
                x,z,var = get_variable(folderName,timeStep,fileName)
                
                if varName == "Cp": var = -np.divide(var-pInfty,0.5*rhoInfty*Uinfty**2)
                if varName == "Cf": var = np.divide(-var,0.5*rhoInfty*Uinfty**2)
                
                varUp = var[z>0]
                varLo = var[z<0]
                xUp = x[z>0]
                xLo = x[z<0]
                
                xUpis = xUp.argsort()
                sorted_xu = xUp[xUpis]
                sorted_vu = varUp[xUpis]
                xLois = xLo.argsort()
                sorted_xl = xLo[xLois]
                sorted_vl = varLo[xLois]

                
                ax.plot(sorted_xl, sorted_vl, color='r',label=folderName+' low',linewidth=1,linestyle=linestyleL)
                ax.plot(sorted_xu, sorted_vu, color='b',label=folderName+' up',linewidth=1,linestyle=linestyleL)
                
            if varName == "Cp":
                ax.plot(expUp[:,0],-expUp[:,1],'bo',label="exp up")
                ax.plot(expLo[:,0],-expLo[:,1],'ro',label="exp low")
            ax.legend()
                
                
            fig.savefig(varName+str(deg).replace(".","")+'.png')
            
    figCl = plt.figure()
    axCl = figCl.add_subplot(1, 1, 1)
    axCl.plot(Clexp[:,0],Clexp[:,1],'ko',label="exp")
            
    figCm = plt.figure()
    axCm = figCm.add_subplot(1, 1, 1)
    axCm.plot(Cmexp[:,0],Cmexp[:,1],'ko',label="exp")
        
    for folderName,colorP in zip(folderList,colorList):
        forecCoeffSim = os.path.join(folderName,forecCoeffSimFile)
        fS = np.genfromtxt(forecCoeffSim,comments="#")
        alphasim = (2.51*np.sin(42.5*fS[:,0]))
        axCl.plot(alphasim,fS[:,3],color=colorP,label=folderName)
        axCm.plot(alphasim,fS[:,5],color=colorP,label=folderName)
        axCl.legend()
        axCm.legend()
        
        
    figCl.savefig("Clmoving.png")
    figCm.savefig("Cmmoving.png")
    
    
if __name__== "__main__":
  main()
    
